# Sauvegarde et restauration d'etcd

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______ 


# Objectif

Nous parlons de la sauvegarde et de la restauration des données du cluster etcd. Voici un bref aperçu de ce que nous couvrons. Nous parlons des raisons pour lesquelles il est important de sauvegarder les données d'etcd ainsi que du processus réel de sauvegarde d'etcd et de restauration de celui-ci.


# Pourquoi sauvegarder les données d'etcd ?

**etcd** est la solution de stockage des données backend d'un cluster Kubernetes. Toute la configuration, tous les objets Kubernetes, applications et configurations sont stockés dans **etcd**. Par conséquent, il est important de pouvoir sauvegarder ces données de cluster en sauvegardant **etcd**. Sinon, si les données **etcd** sont perdues, il faudra reconstruire manuellement toutes les applications Kubernetes.

# Sauvergarde des données d'etcd

Les données d'etcd peuvent être sauvegardées en utilisant l'outil en ligne de commande **etcdctl**. 

La commande **etcdctl snapshot save** permet de sauvegarder les données. Cela donne une idée générale de ce à quoi ressemble la sauvegarde des données d'etcd.

````bash
ETCDCTL_API=3 etcdctl --endpoints $ENDPOINT snapshot save <file name>
````

# Restauration des données d'etcd

Le processus de restauration des données est assez similaire. Les données d'etcd peuvent être restaurées à partir d'une sauvegarde en utilisant la commande etcdctl snapshot restore. 

Il est souvent nécessaire de fournir des paramètres supplémentaires car l'opération de restauration crée un cluster logique temporaire pour fournir les données à partir de ce fichier de sauvegarde.

````bash
ETCDCTL_API=3 etcdctl snapshot restore <file name>
````

# Reférence

https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/#backing-up-an-etcd-cluster